///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//    Counts down (or towards) a significant date
//
// Example:
//    Reference Time:  Wed Mar 20 02:30:12 2013
//    Years: 8  Days: 342  Hours: 15  Minutes: 13  Seconds: 23
//    Years: 8  Days: 342  Hours: 15  Minutes: 13  Seconds: 24
//    Years: 8  Days: 342  Hours: 15  Minutes: 13  Seconds: 25
//    Years: 8  Days: 342  Hours: 15  Minutes: 13  Seconds: 26
//    Years: 8  Days: 342  Hours: 15  Minutes: 13  Seconds: 27
//
// @author Kai Matsusaka <kairem@hawaii.edu>
// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

const int YEAR = 2013;
const int MONTH = 2;
const int DAY = 20;
const int HOUR = 2;
const int MIN = 30;
const int SEC = 12;
const int DST = 0;

int main() {

   struct tm referenceTime;
   
   memset(&referenceTime, 0, sizeof(referenceTime)-1);   //initialize struct to zero

   referenceTime.tm_year = YEAR - 1900;                  //Populate referenceTime
   referenceTime.tm_mon = MONTH;
   referenceTime.tm_mday = DAY;
   referenceTime.tm_hour = HOUR;
   referenceTime.tm_min = MIN;
   referenceTime.tm_sec = SEC;
   referenceTime.tm_isdst = DST;

   time_t refTime = mktime(&referenceTime);            //get struct tm referenceTime in time_t epoch seconds

   if((int)refTime < 0) {
      printf("Date must be after 1970.\n");            //ensure date is after epoch
      exit(EXIT_FAILURE);
   }

   else {
      printf("Reference Time:  %s", asctime(&referenceTime));
   }


   //loop priting out counter every second
   while (true) { 

      time_t now = time(0);            //use to get current time in time_t epoch seconds
      
      double difference = difftime(now, refTime); //returns difference between both times in seconds
      
      int sec = (int)difference;       //cast double to int

      int year = sec / (31536000);     //seconds in a year
      sec = sec % 31536000;

      int day = sec / (86400);         //seconds in a day
      sec = sec % 86400;

      int hour = sec / (3600);         //seconds in an hour
      sec = sec % 3600;

      int min = sec / (60);            //seconds in a minute
      sec = sec % 60;

      printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", year, day, hour, min, sec);

      sleep(1);                        //delays program 1 second
   
   }

   return 0;

}
